# -----------------------------------------------------------------------------------------------------------
# File:		Makefile, for GNU MAKE 4.2
# Purpose:	Keil C51 makefile
# -----------------------------------------------------------------------------------------------------------
# to set in windows environment
# SET TMP = c:\temp
# SET C51INC = c:\Keil\C51\inc;c:\Keil\C51\inc\ADI
# 
# TMP    : Path to use for temporary files generated by the assembler
# C51INC : Path to the folder for C51 include files
# -----------------------------------------------------------------------------------------------------------
toolchain    = KEIL_V809a
#project_path = c:\local\projects\DQAMCNMQ_NEXT
project_path = .
# -----------------------------------------------------------------------------------------------------------
# the expected output
desired_product = DQAMCNMQ_NEXT.hex

# asm_extension = .asm
asm_extension = .a51

asm_source  =
c_source    = DQAMC.c \
              FUNCTION.c
cpp_source  =

# they need to be comma separated starting with comma
libs        = , $(lib_path)\c51s.lib, $(lib_path)\c51fps.lib
# -----------------------------------------------------------------------------------------------------------
out_path    = $(project_path)
inc_path    = $(project_path)
src_path    = $(project_path)
# -----------------------------------------------------------------------------------------------------------
LDFLAGS = RAMSIZE(256)
 
ARFLAGS	= SET(SMALL)\
          VERBOSE\
          DEBUG\
          EP

CCFLAGS	= BROWSE\
          VERBOSE\
          DEBUG\
          LISTINCLUDE\
          OBJECTEXTEND
          
# -----------------------------------------------------------------------------------------------------------
include ..\make\common_keil.mk
# -----------------------------------------------------------------------------------------------------------
# EOF makefile