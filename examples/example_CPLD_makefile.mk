# -----------------------------------------------------------------------------------------------------------
# File:		Makefile , for gnuwin32_msys Make
#
# Purpose:	Xilinx flow makefile, from VHDL to bit stream
#
# -----------------------------------------------------------------------------------------------------------

# this is the top, all the "wanted" files will have this name
product				= cibds_matrix_a
chip				= xc95288xl-6-TQ144

# don't forget to update xst_options.txt
# -p xc95288xl-6-TQ144
# -top cibds_matrix_a
# -ofn "cibds_matrix_a"

arguments_for_cpldfit = -ofmt vhdl -optimize density -loc on -slew fast -init low -inputs 54 -pterms 25 -unused float -power std -terminate keeper

toolchain			= XLX_PLD_XST_ISE_13
# synth ngbuild cpldfit cpldprog

# Directory where our project tree resides
project_location	= c:\local\projects\CIBDS
project_make_path	= $(project_location)\make_a
constraints_file	= $(project_location)\1V0\matrix_a_device\constraints\matrix_a_top.ucf

# if you have netlist files of some cores
cores_path			= $(project_location)\cores

include ..\..\Make\common_fpga.mk

# overwrite it, the default is silent
# xilinx_batch_verbosity = xflow

#-----------------------------------------------------------------------------
# EOF makefile
