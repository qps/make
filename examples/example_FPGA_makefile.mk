# -----------------------------------------------------------------------------------------------------------
# File:		Makefile , for gnuwin32_msys Make
#
# Purpose:	Xilinx flow makefile, from VHDL to bit stream
#
# -----------------------------------------------------------------------------------------------------------

# this is the top, all the "wanted" files will have this name
product			= cibds_monitor
chip			= xc3s1000-5-fg456
#				  xc3s1000-fg456-5  ??? map expect it like this ?
 
# don't forget to update xst_options.txt
# -p xc3s1000-5-fg456
# -top cibds_monitor
# -ofn "cibds_monitor"

toolchain			= XLX_FPGA_XST_ISE_13
# synth ngbuild map par bitgen

# Directory where our project tree resides
project_location	= c:\local\projects\CIBDS
project_make_path	= $(project_location)\make_monitor
constraints_file	= $(project_location)\1V0\monitor_device\constraints\cibds_monitor_top.ucf

# if you have netlist files of some cores
cores_path			= $(project_location)\cores

include ..\..\Make\common_fpga.mk

# overwrite it, the default is silent
xilinx_batch_verbosity = xflow

#-----------------------------------------------------------------------------
# EOF makefile
