# -----------------------------------------------------------------------------------------------------------
# flags than can be used at command line for :
# -----------------------------------------------------------------------------------------------------------
A51 sourcefile [directives...]
A51 @commandfile

ARFLAGS  = VERBOSE \
          NOAMAKE \
          OBJECT($(out_path)\$@) \
          INCDIR($(inc_path)) \
          GEN \
          LIST \
          SET(SMALL) \
          EP \
          XREF \
          PRINT($(out_path)\$(@:.obj=.lst))


COND        Includes (in the listing file) conditional source lines skipped by the preprocessor.
ELSE        Assemble block if the condition of a previous IF is false.
ELSEIF      Assemble block if condition is true and a previous IF or ELSEIF is false.
ENDIF       Ends an IF block.
GEN         Includes all macro expansions in the listing file.
IF          Assemble block if condition is true.
INCLUDE     Includes the contents of another file.
LIST        Includes the assembly source text in the listing file.
NOGEN       Excludes macro expansions from the listing file.
NOLIST      Excludes the assembly source text from the listing file.
NOSYMLIST   Excludes subsequently defined symbols from the symbol table.
REGUSE      Specifies registers modified by the specified function.
RESET       Sets symbols, which may be tested by IF or ELSEIF, to false.
SET         Sets symbols, which may be tested by IF or ELSEIF, to true or a specified value.
SYMLIST     Includes subsequently defined symbols in the symbol table.
# -----------------------------------------------------------------------------------------------------------
c51.exe
CCFLAGS  = VERBOSE \
          OBJECT($(out_path)\$@) \
          WARNINGLEVEL(2) \
          OPTIMIZE(SIZE) \
          INCDIR($(inc_path)) \
          REGFILE($(out_path)\$(@:.obj=.orc)) \
          BROWSE \
          DEBUG \
          CODE \
          LISTINCLUDE \
          SYMBOLS \
          PRINT($(out_path)\$(@:.obj=.lst)) \
          PREPRINT($(out_path)\$(@:.obj=.i))





; ---- debug information -------------
DEBUG DB                Includes debugging information in the object file.
NODEBUG NODB
DEBUGPUBLICS DBP        Add only the publics in the debug information of the object file
OBJECTEXTEND OE         Adds additional debugging information to the object file.
AMAKE AM                Add Automake (Keil) information in object file
NOAMAKE NOAM            Excludes build information from the object file.
BROWSE BR               Enables source browser information. added to the obj file

; ---- Affecting only listing and presentation -------------
VERBOSE                 show extra information
CODE CD                 Includes generated assembly in the listing file.
NOCODE NOCD
PAGELENGTH PL           Specifies the number of lines on a page in the listing file.
PAGEWIDTH PW            Specifies the number of characters on a line in the listing file.
TABS TA                 Specifies the tab character expansion width for the listing file.
EJECT EJ                Inserts a form feed character into the listing file.
LISTINCLUDE LC          Adds contents of include files into the listing file.
NOLISTINCLUDE NOLC
COND CO                 Includes (in the listing file) conditional source lines skipped by the preprocessor.
NOCOND NOCO             Excludes (from the listing file) conditional source lines skipped by the preprocessor.
SYMBOLS SB              Includes a symbol list in the listing file.
NOSYMBOLS NOSB
PRINT PR                Specifies the name for the listing file.
NOPRINT NOPR            Disables listing file generation.
PREPRINT PP             Produces a preprocessor listing file with expanded macros.
NOPREPRINT NOPP
FARWARNING FW           Issue a warning when only 16-bit offset calculations are performed for far addresses.

; ---- Affecting code generation -------------
OBJECT OJ               Specifies the name for the object file.
NOOBJECT NOOJ           Disables object file generation.
OMF2                    Generates an OMF2 object module.
OMF251 O2
SMALL SM                Selects the SMALL memory model.
COMPACT CP              Selects the COMPACT memory model.
LARGE LA                Selects the LARGE memory model.
REGPARMS                Enables passing parameter in registers.
NOREGPARMS              Disables passing parameter in registers.
AREGS                   Enables absolute register (ARn) addressing.
NOAREGS                 Disables absolute register (ARn) addressing.
STUBS ST                Add a call to ?C?FENTRY and ?C?FEXIT for subroutines
GD_DEBUG                Add a call to GD_DEBUG at the exit of subroutines
GD_JUMP(xxx)
GD_LOCSYMS GDL
GD_PARMS
GD_NOPARMS
DISABLE DI              Disables interrupts for the duration of a single function.
RET_PSTK RP             Uses the COMPACT model reentrant stack for return addresses.
RET_XSTK RX             Uses the LARGE model reentrant stack for return addresses.
RET_ISTK RI
ONEREGBANK OB           Generates code for programs that use only one register bank.
VARBANKING VB           Enables far memory type variables.
REGISTERBANK RB         (xxx)
ORDER OR                Allocates storage for variables in the order in which they are declared.
TRIPLES
PTREE
INTERVAL IT             Specifies the interval to use for interrupt vectors.
INTVECTOR IV            Specifies the base address for interrupt vectors.
NOINTVECTOR NOIV        Disables generation of interrupt vectors.
INTPROMOTE IP           Enables ANSI integer promotion rules.
NOINTPROMOTE NOIP       Disables ANSI integer promotion rules.
FLOATFUZZY FF           Sets the number of bits rounded for floating-point comparisons.
ROM                     (SMALL | COMPACT | LARGE) Specifies the ROM model.
EXTEND
NOEXTEND                Disables Cx51 extensions to ANSI C.
ECO51
STRING ST               (CODE | XDATA | FAR) Locates implicit string constants to xdata or far memory.
;---- affecting compiler flow, parameters
SAVE                    Saves settings for AREGS, REGPARMS, and OPTIMIZE directives.
RESTORE                 Restores settings for AREGS, REGPARMS, and OPTIMIZE directives.
SRC                     Creates an assembler source file instead of an object file.
DEFINE DF (XXXXX)       Defines preprocessor names on the command line.
ASM                     Marks the beginning of an inline assembly block.
ENDASM                  Marks the end of an inline assembly block.
MAXARGS MA              Specifies the maximum size of variable-length argument lists.
WARNINGLEVEL WL         Selects the level of warning detection.
INCDIR ID               Sets additional include file paths.
ASMPARMS AP
NOASMPARMS NOAP

OPTIMIZE OT             (0..11, SPEED | SIZE)   Specifies the level of optimization performed by the compiler.
OPTLIMIT OL
OBJECTADVANCED OA       Adds additional information to the object file for linker-level program optimizations.

REGFILE RF              (xxx)
CASE CA
NOCASE NOCA
INTR2 I2
INTR4 I4
MODE2 M2
PARM251 P251
PARM51 P51
HOLD HL                 (xxx)
FIXXC800                Disable generation of MOV dir,dir instruction with two SFR registers.
FIXDRK FD
FIXPOP
NOFIXPOP NFP
CHKXCH
ACALLOPT
CODEPACK                (10)
USERCLASS UCL           Renames memory classes which allows more flexible variable location.
LIST LI
NOLIST NOLI
XCROM XC                Assumes const xdata variables are stored in ROM.
NOPREPROCESS NOP
LINES LN
NOLINES NOL
NAME NA
SDATA
WARNING
MESSAGE
ENUMINT ENI
MOD517                  Enables support for additional hardware features of the Infineon 80C517 and compatible devices.
NOMOD517                Disables support for additional hardware features of the Infineon 80C517 and compatible devices.
BSE
MODAMD
NOMODAMD
MODP2                   Enables dual data pointer support for Philips and Atmel devices.
NOMODP2                 Disables dual data pointer support for Philips and Atmel devices.
MODDP2                  Enables dual data pointer support for the Dallas 320, 520, 530, 550, and compatible devices.
NOMODDP2                Disables dual data pointer support for the Dallas 320, 520, 530, 550, and compatible devices.
MODE2PP M2PP
MODAB2                  Enables dual data pointer support for the Analog Devices Microconverters (ADuC B2 devices).
NOMODAB2                Disables dual data pointer support for the Analog Devices Microconverters (ADuC B2 devices).
MODDA                   Enables arithmetic accelerator support for the Dallas 80C390, 80C400, and 5240.
NOMODDA                 Disables arithmetic accelerator support for the Dallas 80C390, 80C400, and 5240.
MODA2                   Enables dual data pointer support for the Atmel 82x8252 and compatible devices.
NOMODA2                 Disables dual data pointer support for the Atmel 82x8252 and compatible devices.
MODSRC MDS
MODBIN MDB
MODH2
NOMODH2
MODC2
NOMODC2
MDU_F120
NOMDU_F120
MDU_R515
NOMDU_R515
MOD_MX51P MXP
PSOC                    Generate interrupt vectors for Cypress PSoC devices.
# -----------------------------------------------------------------------------------------------------------
bl51.exe
LDFLAGS = RAMSIZE(256)\
          PRINT("$(out_path)\$&.m51")\
          IXREF
# -----------------------------------------------------------------------------------------------------------
BANKx               Specifies the code bank in which to locate the specified segment.
BANKAREA            Specifies the address range where code banks are located.
BIT                 Locates BIT segments.
CODE                Locates CODE segments.
DATA                Locates DATA segments.
DISABLEWARNING      Disables generation of specified warning numbers.
IBANKING            Uses the on-chip code banking hardware of the Mentor M8051EW core, the Infineon SDA30C16x/26x, and the Micronas SDA555x.
IDATA               Locates IDATA segments.
IXREF               Enables cross reference details in the listing file.
NAME                Specifies the module name of the output file.
NOAJMP              Avoids AJMP instructions in bank switch code.
NOAMAKE             Excludes AMAKE information from the object file.
NODEBUGLINES        Excludes line number information from the output object file.
NODEBUGPUBLICS      Excludes public symbol information from the output object file.
NODEBUGSYMBOLS      Excludes local symbol information from the output object file.
NODEFAULTLIBRARY    Disables automatic inclusion of run-time library routines.
NOINDIRECTCALL      Disables bank switching code for indirectly called functions.
NOJMPTAB            Disables generation of the code banking jump table.
NOLINES             Excludes line number information from the listing file and object file.
NOMAP               Excludes the memory map from the listing file.
NOOVERLAY           Disables data segment overlaying.
NOPRINT             Disables generation of the listing (MAP) file.
NOPUBLICS           Excludes public symbols from the listing file and object file.
NOSORTSIZE          Disables sorting sections by size prior to locating.
NOSYMBOLS           Excludes local symbols from the listing file and object file.
OVERLAY             Modifies the call tree for overlaying of local bit and data segments.
PAGELENGTH          Specifies the number of lines on a page in the listing file.
PAGEWIDTH           Specifies the number of characters on a line in the listing file.
PDATA               Locates PDATA segments.
PRECEDE             Locates segments in DATA memory prior to other relocatable BIT, DATA, and IDATA segments.
PRINT               Specifies the name of the listing file.
RAMSIZE             Specifies the number of bytes of DATA and IDATA.
RECURSIONS          Specifies the maximum number of recursive calls detected before link failure.
REGFILE             Specifies the name of the register definition file for global register coloring.
RTX51               Specifies that the RTX51 Real-Time Kernel is used.
RTX51TINY           Specifies that the RTX51 Tiny Real-Time Kernel is used.
SPEEDOVL            Ignores references from constant segments to program segments during overlay analysis.
STACK               Locates segments in the uppermost IDATA memory space.
VERBOSE             (*) is not valid for this linker
XDATA               Locates XDATA segments.
# -----------------------------------------------------------------------------------------------------------
LIB51 LIBRARY MANAGER V4.24
Add      {file[(module[,...])]} [,...] TO library_file
Create   library_file
Delete   library_file(module[,...])
Exit
eXtract  library_file(module) TO file
Help
List     library_file [TO file] [PUBLICS]
Replace  {file[(module[,...])]} [,...] IN library_file
Transfer {file[(module[,...])]} [,...] TO library_file
# -----------------------------------------------------------------------------------------------------------
