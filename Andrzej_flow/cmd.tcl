#script for Synplify with IGLOO2
#A.Skoczen - 12.2014
#A.Skoczen - revision on 3.2015
#
#Requirements:
# VHDL RTL files - directory ./rtl
# timing constraints file - file ./constraints/'top_module_name'.fdc 
# results directory - empty directory ./synplify
#
#Results:
# synthesised design - file ./synplify/rev_1/'work_lib_name'.edn
# logfile - file ./synplify/xx.log
# 
#A.Skoczen - February 2016 - added config file for variables and path
set fp [open "./config.cfg" r]

while { [gets $fp data] >= 0 } {
   #puts $data
   set tab [split $data =]
   set pattern [lindex $tab 0]
   switch $pattern {
	proj_name {set proj_name [lindex $tab 1]
				puts "Project: $proj_name"}
	top_name {set top_name [lindex $tab 1]
				puts "Top module: $top_name"}
	lib_name {set lib_name [lindex $tab 1]
				puts "Library: $lib_name"}
	tech {set tech [lindex $tab 1]
				puts "FPGA: $tech"}
	}
}
close $fp

set work $lib_name
set uart_lib COREUART_LIB
###############################

project -new ./synplify/$proj_name.prj
project -save $proj_name.prj

puts "$uart_lib source files:"
set rtllist [glob ./rtl/COREUART_LIB/*.vhd]
foreach f $rtllist {
puts $f
add_file -vhdl -lib $uart_lib $f
}

set rtllist [glob ./rtl/src/*.vhd]
puts $rtllist
foreach f $rtllist {
puts $f
add_file -vhdl -lib $work $f
}

set file_fdc [format ./constraints/%s.fdc $top_name]
if { [file exists $file_fdc] } {
	add_file -constraint $file_fdc
	puts "as: Timing constraint file: $file_fdc loaded"
} else {
	puts "as: Lack of timing constraint file: $file_fdc"
	exit
}
impl -add synthesis -type fpga

set_option -top_module [format %s.%s $work $top_name]

switch $tech {
	IG2 {
		puts "as: IGLOO2"
		set_option -technology IGLOO2
		set_option -part M2GL010T
		set_option -grade -1
		set_option -package FBGA484
	}
	SF2 {
		puts "as: SmartFusion2"
		set_option -technology SmartFusion2
		set_option -part M2S010
		set_option -grade -1
		set_option -package FBGA484
	}
	default {
		puts "as: Lack of technology settings"
		exit
	}
}

set_option -frequency 50.0
#set_option -vlog_std v2001
set_option -vhdl2008 1
#set_option -write_verilog 1
set_option -write_vhdl 1
set_option -report_path 400
#set_option -retiming 1
#--------------------------------------------

#---------------------------------------------
project -save  $proj_name.prj
#start synthesis
project -run 
#synthesis

project -save $proj_name.prj

