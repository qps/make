# -----------------------------------------------------------------------------------------------------------
# File:		Makefile
#
# Purpose:	Xilinx flow makefile, from VHDL to bit stream
#
# make clean        will delete /tmp and create it again
# make all			will run [synth ngbuild cpldfit cpldprog] or [synth ngbuild map par bitgen]
# make synth		will run xst.exe and produce $(product).ngc $(product).srp
# make synp		    will run synplifi and produce $(product).ngc $(product).srp
# make ngbuild		will run ngbuild.exe and produce $(product).ngd $(product).bld
# make cpldfit		will run cpldfit.exe and produce
# make cpldprog     will run hprep6.exe and produce 
# make impact		will run impact.exe and produce $(product).xsvf
# make map			will run map.exe and produce
# make par			will run par.exe and produce
# make bitgen		will run bitgen.exe and produce $(product).bit
# make trace		will run trace.exe and produce
# make promgen		will run promgen.exe and produce $(product).mcs/.prm/.sig/.cfi
# make coregen		will run coregen.exe and produce
#
# -----------------------------------------------------------------------------------------------------------

# the following are expected to be defined by the project makefile

# product			: this is the top, all the "wanted" files will have this name
# chip				: xilinx part like: xc95288xl-6-TQ144
# toolchain			: which toolchain to use :
#						XLX_PLD_XST_ISE_9
# 						XLX_FPGA_XST_ISE_9
#						XLX_PLD_XST_ISE_13
# 						XLX_FPGA_XST_ISE_13
#                       LIBERO_SYNP_11_7

# project_location	: Directory where the project tree resides
# project_make_path	: normally it is $(project_location)/make_xxxx
# cores_path		: where to look for cores netlist
# constraints_file	: normally is $(project_sources_path)/$(product).ucf

# arguments_for_cpldfit : normally "-ofmt vhdl -optimize density -loc on -slew fast -init low -inputs 54 -pterms 25 -unused float -power std -terminate keeper"

# -----------------------------------------------------------------------------------------------------------
# to check if the directory exist
#
#	[ -d $(project_tmp_path) ] || $(mingw_bin_path)/mkdir $(project_tmp_path) 
# old versions of MingW don't support
# in that case you casn use
# 	$(mingw_bin_path)/test -d $(project_tmp_path) || $(mingw_bin_path)/mkdir $(project_tmp_path) 
#
# windows style
#	@if NOT exist $(project_tmp_path)\ (mkdir $(project_tmp_path))
#	@if NOT exist $(project_tmp_path)\ (mkdir $(project_tmp_path)) 
# -----------------------------------------------------------------------------------------------------------

project_sources_path	= $(project_location)\src

# Directory where we put all the files generated during the build process, deleted later
project_tmp_path		= $(project_make_path)\tmp

options_for_xst			= $(project_make_path)\xst_options.txt
options_for_ngdbuild	= $(project_make_path)\ngdbuild_options.txt
options_for_map			= $(project_make_path)\map_options.txt
options_for_par			= $(project_make_path)\par_options.txt
options_for_trace		= $(project_make_path)\trace_options.txt
options_for_bitgen		= $(project_make_path)\bitgen_options.txt
options_for_promgen		= $(project_make_path)\promgen_options.txt
commands_for_impact		= $(project_make_path)\impact.cmd

# [ise|xflow|silent]
xilinx_batch_verbosity	= silent

vpath %.vhd $(project_sources_path)
# vpath %.ucf $(project_sources_path)

    mingw_bin_path  = c:\local\projects\Make\bin
#	mingw_bin_path  = c:\xlx_i9\EDK\cygwin\bin	# this one generated recursion (?) problems under Windows 7
#	mingw_bin_path  = c:\xlx_i13\ISE_DS\EDK\gnuwin\bin 
#   mingw_bin_path  = c:\xlx_i14\14.7\ISE_DS\EDK\gnuwin\bin


ifeq ($(toolchain), Andrzej)
	toolchain_flow = andrzej_flow
	toolchain_defined = true
endif 


    
ifeq ($(toolchain),LIBERO_SYNP_11_7)
	toolchain_flow = synp_pro
    synplify_bin_path = c:\Microsemi\Libero_SoC_v11.7\Synplify\bin64\mbin
#   synplify_bin_path = c:\Microsemi\Libero_SoC_v11.7\Synplify\bin for Synplify_Pro.exe

	# Export environment variables used by the Microsemi tool chain

    libero_bin_path = c:\Microsemi\Libero_SoC_v11.7\Designer\bin
#	export TCL_LIBRARY=c:\Microsemi\Libero_SoC_v11.7\Designer\lib\tcl8.5
	
    libero_bin64_path = c:\Microsemi\Libero_SoC_v11.7\Designer\bin64
	export TCL_LIBRARY=c:\Microsemi\Libero_SoC_v11.7\Designer\lib64\tcl8.5
    
	export SYN_WRXML=1
	
	export ACTEL_SW_DIR=c:\Microsemi\Libero_SoC_v11.7\Designer
	export AMHOME=c:\Microsemi\Libero_SoC_v11.7\Designer\am

	export MICROSEMI_ROOT_PORT=c:\local\projects\uQDS\tooldata\libero.5636
	 
	toolchain_defined = true
endif 

ifeq ($(toolchain),XLX_PLD_XST_ISE_9)
	toolchain_flow = synth ngbuild cpldfit cpldprog

	xlx_bin_path	= c:\xlx_i9\bin\nt64
#	xlx_bin_path	= c:\xlx_i9\bin\nt
	# Export environment variables used by the Xilinx tool chain

	export XILINX=c:\xlx_i9
	export XILINX_EDK=c:\xlx_i9\EDK
	export LMC_HOME=c:\xlx_i9\smartmodel\nt\installed_nt
	export XILINX_CD=c:\xlx_i9
	export NO_XILINX_DATA_LICENSE=HIDDEN
	export XILINXD_LICENSE_FILE=2112@lxlicen01,2112@lxlicen02,2112@lxlicen03
	toolchain_defined = true
endif 

ifeq ($(toolchain),XLX_FPGA_XST_ISE_9)
	toolchain_flow = synth ngbuild map par bitgen
	
	xlx_bin_path	= c:\xlx_i9\bin\nt64
#	xlx_bin_path	= c:\xlx_i9\bin\nt

	# Export environment variables used by the Xilinx tool chain

	export XILINX=c:\xlx_i9
	export XILINX_EDK=c:\xlx_i9\EDK
	export LMC_HOME=c:\xlx_i9\smartmodel\nt\installed_nt
	export XILINX_CD=c:\xlx_i9
	export NO_XILINX_DATA_LICENSE=HIDDEN
	export XILINXD_LICENSE_FILE=2112@lxlicen01,2112@lxlicen02,2112@lxlicen03
	toolchain_defined = true
endif 

ifeq ($(toolchain),XLX_PLD_XST_ISE_13)
	toolchain_flow = synth ngbuild cpldfit cpldprog

	xlx_bin_path	= c:\xlx_i13\ISE_DS\ISE\bin\nt64
	
	# Export environment variables used by the Xilinx tool chain

	export XILINX=c:\xlx_i13\ISE_DS\ISE
	export XILINX_CD=c:\xlx_i13
	export XILINXD_LICENSE_FILE=2112@lxlicen01,2112@lxlicen02,2112@lxlicen03
	export LM_LICENSE_FILE=c:\xlx_i13\ISE_DS\ISE\data\
	export NO_XILINX_DATA_LICENSE=HIDDEN
	export XILINX_EDK=c:\xlx_i13\ISE_DS\EDK
	export XILINX_DSP=c:\xlx_i13\ISE_DS\ISE
	export XILINX_PLANAHEAD=c:\xlx_i13\ISE_DS\PlanAhead
	toolchain_defined = true
endif 

ifeq ($(toolchain),XLX_FPGA_XST_ISE_13)
	toolchain_flow = synth ngbuild map par bitgen
	
	xlx_bin_path	= c:\xlx_i13\ISE_DS\ISE\bin\nt64

	# Export environment variables used by the Xilinx tool chain

	export XILINX=c:\xlx_i13\ISE_DS\ISE
	export XILINX_CD=c:\xlx_i13
	export XILINXD_LICENSE_FILE=2112@lxlicen01,2112@lxlicen02,2112@lxlicen03
	export LM_LICENSE_FILE=c:\xlx_i13\ISE_DS\ISE\data\
	export NO_XILINX_DATA_LICENSE=HIDDEN
	export XILINX_EDK=c:\xlx_i13\ISE_DS\EDK
	export XILINX_DSP=c:\xlx_i13\ISE_DS\ISE
	export XILINX_PLANAHEAD=c:\xlx_i13\ISE_DS\PlanAhead
	toolchain_defined = true
endif 

ifeq ($(toolchain),XLX_PLD_XST_ISE_14)
	toolchain_flow = synth ngbuild cpldfit cpldprog

	xlx_bin_path	= c:\xlx_i14\14.7\ISE_DS\ISE\bin\nt64

	# Export environment variables used by the Xilinx tool chain

	export XILINX=c:\xlx_i14\14.7\ISE_DS\ISE
	export XILINX_CD=c:\xlx_i14\14.7
	export XILINXD_LICENSE_FILE=2112@lxlicen01,2112@lxlicen02,2112@lxlicen03
	export LM_LICENSE_FILE=c:\xlx_i14\14.7\ISE_DS\ISE\data\
	export NO_XILINX_DATA_LICENSE=HIDDEN
	export XILINX_EDK=c:\xlx_i14\14.7\ISE_DS\EDK
	export XILINX_DSP=c:\xlx_i14\14.7\ISE_DS\ISE
	export XILINX_PLANAHEAD=c:\xlx_i14\14.7\ISE_DS\PlanAhead
	toolchain_defined = true
endif 

ifeq ($(toolchain),XLX_FPGA_XST_ISE_14)
	toolchain_flow = synth ngbuild map par bitgen
	
	xlx_bin_path	= c:\xlx_i14\14.7\ISE_DS\ISE\bin\nt64
	
	# Export environment variables used by the Xilinx tool chain

	export XILINX=c:\xlx_i14\14.7\ISE_DS\ISE
	export XILINX_CD=c:\xlx_i14\14.7
	export XILINXD_LICENSE_FILE=2112@lxlicen01,2112@lxlicen02,2112@lxlicen03
	export LM_LICENSE_FILE=c:\xlx_i14\14.7\ISE_DS\ISE\data\
	export NO_XILINX_DATA_LICENSE=HIDDEN
	export XILINX_EDK=c:\xlx_i14\14.7\ISE_DS\EDK
	export XILINX_DSP=c:\xlx_i14\14.7\ISE_DS\ISE
	export XILINX_PLANAHEAD=c:\xlx_i14\14.7\ISE_DS\PlanAhead
	toolchain_defined = true
endif 

# to check if the directory exist

# old versions of MingW don't support
#	@if NOT exist $(project_tmp_path)\ (mkdir $(project_tmp_path))

# this is why I used
# 	$(mingw_bin_path)\test -d $(project_tmp_path) || mkdir $(project_tmp_path) 

#-----------------------------------------------------------------------------
# default: synth

# this is the minimal toolchain flow from VHD to JED or BIT
all: $(toolchain_flow)

# this is extended andrzej flow

#andrzej_synth_tmr: $(toolchain_flow)

# synth: $(product).ngc $(product).srp
# synp: 
# ngbuild: $(product).ngd $(product).bld

# map: $(product)_map.ncd $(product).pcf
# par: $(product).ncd $(product).unroutes $(product).par $(product).twr
# trace: $(product).ncd $(product).unroutes $(product).par $(product).twr
# bitgen: $(product).bit

# cpldfit: $(product)_map.ncd $(product).cxt
# cpldprog: $(product).bit

# impact: 
# promgen:
# coregen: 

.SECONDARY: $(product).ngc $(product).srp $(product).ngd $(product).bld $(product)_map.ncd 
.SECONDARY: $(product).pcf $(product).ncd $(product).unroutes $(product).par $(product).twr

#-----------------------------------------------------------------------------

ifneq ($(toolchain_defined),)
	@echo "Toolchain not defined"
endif 

#-----------------------------------------------------------------------------
# For Xilinx FPGAs and CPLDs
# export XIL_XST_HIDEMESSAGES =
# = none 				(default) Maximum verbosity. All messages are printed out.
# = hdl_level 			Reduce verbosity during VHDL or Verilog Analysis and HDL Basic and Advanced Synthesis.
# = low_level 			Reduce verbosity during Low-level Synthesis.
# = hdl_and_low_levels	Reduce verbosity at all stages.


# This rule is triggered if any of the HDL files are changed or the synthesis options are changed.

#%.ngc %.srp: $(vhdl_files) $(options_for_xst)
synth: $(options_for_xst)
	@echo "========================================================================"
	@echo "Synthesize with XST [*.vhd,files_for_synthesis.txt] -> [*.ngc,*.ser,...]"
	@echo "========================================================================"
	@if NOT exist $(project_tmp_path)\ (mkdir $(project_tmp_path))
	@cd $(project_tmp_path) && \
	$(xlx_bin_path)\xst.exe \
						-intstyle $(xilinx_batch_verbosity) \
						-ifn $(options_for_xst) 
	@echo "======================== Done =========================================="

#-----------------------------------------------------------------------------
# This rule is triggered if
 
synp:
	@echo "========================================================="
	@echo "Synthesize with Synplify [*.vhd] -> [*.ngc]"
	@echo "========================================================="
	@if NOT exist $(project_tmp_path)\ (mkdir $(project_tmp_path))
	@cd $(project_tmp_path) && \
	$(xlx_bin_path)\synplify_premier_dp.exe \
										-batch \
										$(project_tmp_path)\$(product).tcl
	@echo "======================== Done =========================================="
	
	
	
#-----------------------------------------------------------------------------
# This rule is triggered if
 
andrzej_flow:
	@echo "========================================================="
	@echo "Synthesize with Andrzej"
	@echo "========================================================="
	@powershell .\igloo2.ps1 synth
#	@powershell .\igloo2.ps1 impl
	@echo "======================== Done =========================================="
	
#-----------------------------------------------------------------------------
# This rule is triggered if
 
andrzej_synth_tmr:
	@echo "========================================================="
	@echo "Synthesize with Andrzej - tmr option"
	@echo "========================================================="
	@powershell .\igloo2.ps1 synth tmr
	@echo "======================== Done =========================================="
	
#-----------------------------------------------------------------------------
# This rule is triggered if

andrzej_impl:
	@echo "========================================================="
	@echo "Synthesize with Andrzej - impl option"
	@echo "========================================================="
	@powershell .\igloo2.ps1 impl
	@echo "======================== Done =========================================="
	
#-----------------------------------------------------------------------------


# This rule is triggered if

synp_pro:
	@echo "========================================================="
	@echo "Synthesize with Synplify Pro [*.vhd] -> [*.edn]"
	@echo "========================================================="
	@if NOT exist $(project_tmp_path)\ (mkdir $(project_tmp_path))
	@cd $(project_tmp_path) && \
    $(synplify_bin_path)\synplify.exe \
										$(project_location)\synthesis\$(product).prj \
	                                    -lcv:rySTgnrD \
	                                    -licensetype synplifypro_actel \
										-verbose_log \
										-log $(project_tmp_path)\synplify.log \
										-batch

	@echo "========================================================================"
# type is a DOS or windows CMD internal command
# tip: eclosing the path between " makes it accept unix style								
	type "$(project_tmp_path)"\synplify.log
#   $(mingw_bin_path)\cat $(project_tmp_path)\synplify.log
	
	@echo "======================== Done =========================================="

#-----------------------------------------------------------------------------
# This rule is triggered if
#   $(libero_bin_path)\designer.exe \

libero:
	@echo "========================================================="
	@echo "Implement with Libero  [*.edn] -> [*.abc]"
	@echo "========================================================="
	@if NOT exist $(project_tmp_path)\ (mkdir $(project_tmp_path))
	@cd $(project_tmp_path) && \
    $(libero_bin_path)\libero.exe \
                                 SCRIPT:$(project_location)\igloo2.tcl \
                                 LOGFILE:$(project_tmp_path)\libero.log

	@echo "========================================================================"
# type is a DOS or windows CMD internal command
# tip: eclosing the path between " makes it accept unix style								
	type "$(project_tmp_path)"\libero.log
#   $(mingw_bin_path)\cat $(project_tmp_path)\libero.log
	
	@echo "======================== Done =========================================="
#-----------------------------------------------------------------------------
# This rule is triggered if
#   $(libero_bin_path)\designer.exe \
#   $(libero_bin_path)\designer.exe \

libero_tcl:
	@echo "========================================================="
	@echo "Implement with Libero  [*.tcl] -> [*.abc]"
	@echo "========================================================="
	@if NOT exist $(project_tmp_path)\ (mkdir $(project_tmp_path))
	@cd $(project_tmp_path) && \
    $(libero_bin64_path)\acttclsh.exe \
                                 $(project_location)\igloo2.tcl \
                                 LOGFILE:$(project_tmp_path)\libero.log

	@echo "========================================================================"
# type is a DOS or windows CMD internal command
# tip: eclosing the path between " makes it accept unix style								
	type "$(project_tmp_path)"\libero.log
#   $(mingw_bin_path)\cat $(project_tmp_path)\libero.log
	
	@echo "======================== Done =========================================="
#-----------------------------------------------------------------------------
# For Xilinx FPGAs and CPLDs
# This rule is triggered if 

#%.ngd %.bld: %.ngc $(constraints_file) $(options_for_ngdbuild)
ngbuild: $(options_for_ngdbuild)
	@echo "========================================================="
	@echo "Translate (ngbuild) *.ngc -> [*.ngd,*.bld]"
	@echo "========================================================="
	@if NOT exist $(project_tmp_path)\ (mkdir $(project_tmp_path))
	@cd $(project_tmp_path) && \
	$(xlx_bin_path)\ngdbuild.exe \
							-intstyle $(xilinx_batch_verbosity) \
							-p $(chip) \
							-verbose \
							-f $(options_for_ngdbuild) \
							-uc $(constraints_file) \
							-dd $(project_tmp_path) \
							-sd $(cores_path) \
							$(project_tmp_path)\$(product).ngc \
							$(project_tmp_path)\$(product).ngd
	@echo "======================== Done =========================================="
					
#-----------------------------------------------------------------------------
# For Xilinx FPGAs and CPLDs
# This rule is triggered if *.bit or impact_cmd_file change.

impact: $(product).bit $(commands_for_impact) 
	@echo "========================================================="
	@echo "Download into PLD with Impact *.bit -> PLD"
	@echo "========================================================="
	$(xlx_bin_path)\impact.exe -batch \
						       -b $(project_tmp_path)\$(product).bit \
						       $(commands_for_impact)

	@echo "======================== Done =========================================="

#-----------------------------------------------------------------------------
# For Xilinx FPGAs
# This rule is triggered if *.ngd file change. 

#%_map.ncd %.pcf: %.ngd $(options_for_map)
map: $(options_for_map)
	@echo "========================================================="
	@echo "Map *.ngd -> [*_map.ncd,*.pcf]"
	@echo "========================================================="
	@if NOT exist $(project_tmp_path)\ (mkdir $(project_tmp_path))
	@cd $(project_tmp_path) && \
	$(xlx_bin_path)\map.exe \
						-intstyle $(xilinx_batch_verbosity) \
						-p $(chip) \
						-w \
						-f $(options_for_map) \
						-o $(project_tmp_path)\$(product)_map.ncd \
						$(project_tmp_path)\$(product).ngd \
						$(project_tmp_path)\$(product).pcf

	@echo "========================================================================"
# type is a DOS or windows CMD internal command
# tip: eclosing the path between " makes it accept unix style								
	type "$(project_tmp_path)"\$(product)_map.mrp
#   $(mingw_bin_path)\cat $(project_tmp_path)\$(product)_map.mrp
	@echo "======================== Done =========================================="

#-----------------------------------------------------------------------------
# For Xilinx FPGAs
# This rule is triggered if 

#%.ncd %.unroutes %.par %.twr: %_map.ncd $(product).pcf
par: $(options_for_par)
	@echo "========================================================="
	@echo "Place and Route [*.ngc,*.pcf] -> []"
	@echo "========================================================="
	@if NOT exist $(project_tmp_path)\ (mkdir $(project_tmp_path))
	@cd $(project_tmp_path) && \
	$(xlx_bin_path)\par.exe \
						-intstyle $(xilinx_batch_verbosity) \
						-w \
						-f $(options_for_par) \
						$(project_tmp_path)\$(product)_map.ncd \
						$(project_tmp_path)\$(product).ncd \
						$(project_tmp_path)\$(product).pcf 
	@echo "======================== Done =========================================="

#-----------------------------------------------------------------------------
# For Xilinx FPGAs
# This rule is triggered if any of the NCD files or *.unroutes *.twr or ut file change.
# you need to check that there is no *.unroutes files so all signals are completely routed
# you need to check that there is no *.twr files, timing errors

# %.bit: %.ncd $(product).unroutes $(product).twr $(options_for_bitgen)
bitgen: $(options_for_bitgen)
	@echo "========================================================="
	@echo "Bitgen *.ncd -> *.bit"
	@echo "========================================================="
	@if NOT exist $(project_tmp_path)\ (mkdir $(project_tmp_path))
	@cd $(project_tmp_path) && \
	$(xlx_bin_path)\bitgen.exe \
							-intstyle $(xilinx_batch_verbosity) \
							-f $(options_for_bitgen) \
							$(project_tmp_path)\$(product).ncd
	@echo "======================== Done =========================================="

#-----------------------------------------------------------------------------
# To implement
# example
# XSLTProcess.exe -ise "ise_prj" $(product)_build.xml
ToDo1:
	@echo "========================================================="
	@if NOT exist $(project_tmp_path)\ (mkdir $(project_tmp_path))
	@cd $(project_tmp_path) && \
	$(xlx_bin_path)\xsltprocess.exe \
							-ise "ise_prj" \
							$(product)_build.xml
	@echo "======================== Done =========================================="

#-----------------------------------------------------------------------------
# To implement
# example
# tsim.exe -ise "ise_prj" -intstyle ise $(product) $(product).nga
ToDo2:
	@echo "========================================================="
	@echo "Bitgen *.ncd -> *.bit"
	@echo "========================================================="
	@if NOT exist $(project_tmp_path)\ (mkdir $(project_tmp_path))
	@cd $(project_tmp_path) && \
	$(xlx_bin_path)\tsim.exe \
							-ise "ise_prj" \
							-intstyle $(xilinx_batch_verbosity) \
							$(product) \
							$(product).nga
	@echo "======================== Done =========================================="

#-----------------------------------------------------------------------------
# To implement
# example
# taengine.exe -ise "ise_prj" -intstyle ise -f $(product) -w --format html1 -l $(project_location)\matrix_a_device\VHDL\ise_project\9_1_03i\cibg_a_matrix_html\tim\timing_report.htm
ToDo3:
	@echo "========================================================="
	@echo "Bitgen *.ncd -> *.bit"
	@echo "========================================================="
	@if NOT exist $(project_tmp_path)\ (mkdir $(project_tmp_path))
	@cd $(project_tmp_path) && \
	$(xlx_bin_path)\taengine.exe \
							-ise "ise_prj" \
							-intstyle $(xilinx_batch_verbosity) \
							-f $(product) \
							-w \
							--format html1 \
							-l timing_report.htm							
	@echo "======================== Done =========================================="

#-----------------------------------------------------------------------------
# This rule is triggered if  

cpldfit:
	@echo "========================================================="
	@echo "CPLD fit *.ngd -> [*.vm6,*.gyd,*.rpt,*.pnx,*.mdf,*.cxt]"
	@echo "========================================================="
	@if NOT exist $(project_tmp_path)\ (mkdir $(project_tmp_path))
	@cd $(project_tmp_path) && \
	$(xlx_bin_path)\cpldfit.exe \
							-p $(chip) \
							-intstyle $(xilinx_batch_verbosity) \
							$(arguments_for_cpldfit) \
							$(project_tmp_path)\$(product).ngd
	@echo "======================== Done =========================================="
							 
#-----------------------------------------------------------------------------
# For Xilinx CPLDs
# -autosig										Automatically Generate Signature
# -intstyle [silent|xflow|ise]
# -n											Specify Signature Value for Readback
# -nopullup										Disable Pullups
# -s [ieee1532|IEEE1532|ieee1149|IEEE1149]		Produce ISC File
# -tmv file_name								Specify Test Vector File

# This rule is triggered if
  
cpldprog:
	@echo "========================================================="
	@echo "CPLD Programming File *.vm6 -> [*.isc,*.jed]"
	@echo "========================================================="
	@if NOT exist $(project_tmp_path)\ (mkdir $(project_tmp_path))
	@cd $(project_tmp_path) && \
	$(xlx_bin_path)\hprep6.exe \
							-i $(project_tmp_path)\$(product).vm6 \
							-s IEEE1149 \
							-n $(project_tmp_path)\$(product) 
	@echo "======================== Done =========================================="

#-----------------------------------------------------------------------------
# For Xilinx FPGAs
# This rule is triggered if 

#%.ncd %.unroutes %.par %.twr: %_map.ncd $(product).pcf
trace: $(options_for_trace)
	@echo "=============================================================="
	@echo "Timing Reporter And Circuit Evaluator [*.ngd,*.pcf] -> [*.twr]"
	@echo "=============================================================="
	@if NOT exist $(project_tmp_path)\ (mkdir $(project_tmp_path))
	@cd $(project_tmp_path) && \
	$(xlx_bin_path)\trce.exe \
						-intstyle $(xilinx_batch_verbosity) \
						-f $(options_for_trace) \
						$(project_tmp_path)\$(product).ncd \
						$(project_tmp_path)\$(product).pcf 
	@echo "======================== Done =========================================="
#	trce -e $*.ncd $*.pcf
#-----------------------------------------------------------------------------
# For Xilinx FPGAs
promgen: $(options_for_promgen)
	@echo "========================================================="
	@echo "Prom Generate *.bit -> *.mcs"
	@echo "========================================================="
	@if NOT exist $(project_tmp_path)\ (mkdir $(project_tmp_path))
	@cd $(project_tmp_path) && \
	$(xlx_bin_path)\promgen.exe \
							-f $(options_for_promgen) \
							-u 0 $(project_tmp_path)\$(product).bit \
							-x $(bitstream_prom) \
							-o $(project_tmp_path)\$(product)
	@echo "======================== Done =========================================="

#-----------------------------------------------------------------------------
# For Xilinx FPGAs
coregen: 
	@echo "========================================================="
	@echo "CoreGen"
	@echo "========================================================="
	@if NOT exist $(project_tmp_path)\ (mkdir $(project_tmp_path))
	@cd $(project_tmp_path) && \
	$(xlx_bin_path)\coregen.exe \
							-b chipscope_icon.xco \
							-p coregen.cgp
	@echo "======================== Done =========================================="

#-----------------------------------------------------------------------------
# file name extensions used by xilinx
# .bgn .bit _bitgen.xwbt .bld .drc .lso _map.map _map.mrp _map.ncd
# _map.ngm _map.xrpt .ncd .ngc .ngd _ngdbuild.xrpt .ngr .pad
# _pad.csv _pad.txt .par _par.xrpt .pcf .ptwx .srp _summary.xml
# .twr .twr_pad.txt .twx .unroutes _usage.xml .xpi _xst.xrpt

clean:
	@if exist $(project_tmp_path)\ (rmdir /S /Q $(project_tmp_path))
	@echo "======================== Done =========================================="

#	mkdir tmp

#-----------------------------------------------------------------------------
# EOF makefile